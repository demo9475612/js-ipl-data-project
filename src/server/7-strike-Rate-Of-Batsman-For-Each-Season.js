const csvToJsonConvertion = require("../csvToJson");
const fs = require('fs');

const matchCsvFilePath= '../data/matches.csv';
const deliveriesCsvFilePath = '../data/deliveries.csv';
const outputFilePath = '../public/output/strikeRateOfBatsmanForEachSeason.json';

function getStrikeRateOfBatsmanPerSeason(){
    try{ 
       
        csvToJsonConvertion(matchCsvFilePath).then((matchData)=>{
            const matchIdToSeason = {};
            for(index =0 ;index<matchData.length;index++){
                const match = matchData[index];
                matchIdToSeason[match.id]=match.season;
            }
           // console.log(matchIdToSeason);
           csvToJsonConvertion(deliveriesCsvFilePath).then((deliveriesData)=>{
                
                const bastmanStrikeRatePerSeason = {};
                for(let index=0;index<deliveriesData.length;index++){
                    const delivery = deliveriesData[index];
                    const matchId = delivery.match_id;
                    const season = matchIdToSeason[matchId];
                    const batsman = delivery.batsman;
                    const runs = parseInt(delivery.batsman_runs);
                    const extra = parseInt(delivery.extra_runs);
                    const isWide = delivery.wide_runs && parseInt(delivery.wide_runs) > 0;
                    const isNoBall = delivery.noball_runs && parseInt(deliveriesData.noball_runs) > 0;

                    if(season && !isWide && !isNoBall){
                        if(!bastmanStrikeRatePerSeason[season]){
                            bastmanStrikeRatePerSeason[season]= {};
                        }
                        if(!bastmanStrikeRatePerSeason[season][batsman]){
                            bastmanStrikeRatePerSeason[season][batsman]={runs:runs , balls:1};
                        }else{
                            bastmanStrikeRatePerSeason[season][batsman].runs +=runs;
                            bastmanStrikeRatePerSeason[season][batsman].balls++;
                        }
                    }
                }
                const strikeRate ={};
                
                for(let season in bastmanStrikeRatePerSeason){
                     for(let batsman in bastmanStrikeRatePerSeason[season]){
                        const {runs , balls} = bastmanStrikeRatePerSeason[season][batsman];
                        const strR=((runs/balls)*100).toFixed(2);
                        if(!strikeRate[season]){
                            strikeRate[season]={};
                        }
                        if(strikeRate[season])
                        strikeRate[season][batsman]=strR;
                     }
                } 
                fs.writeFileSync(outputFilePath,JSON.stringify(strikeRate,null,2));
                console.log('Strike rate of each batsman in every season is Written to file');
               
               
                 
                
           });
        });
    }catch(error){
        console.error("Error Found:",error);
    }
}
getStrikeRateOfBatsmanPerSeason();