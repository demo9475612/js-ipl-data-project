const { indexOf } = require("lodash");
const csvToJsonConvertion = require("../csvToJson");
const fs = require('fs');

const matchCsvFilePath= '../data/matches.csv';
const outputFilePath = '../public/output/teamWonTossWonMatch.json';

function getTeamWonTossWonMatch(){
    try{
        const matchData= csvToJsonConvertion(matchCsvFilePath).then((matchData)=>{
            const tossWonMatchWon ={};
            for(let index=0;index< matchData.length;index++){
                const season = matchData[index].season;
                const tosswinner = matchData[index].toss_winner;
                const matchWinner = matchData[index].winner;
                
                if(tosswinner ==matchWinner){
                    if(!tossWonMatchWon[season]){
                        tossWonMatchWon[season] = {};
                    }
                    if(!tossWonMatchWon[season][matchWinner]){
                        tossWonMatchWon[season][matchWinner] = 1;
                    } else{
                        tossWonMatchWon[season][matchWinner]++;
                    }
                }
            }
            //console.log(tossWonMatchWon);
            fs.writeFileSync(outputFilePath,JSON.stringify(tossWonMatchWon,null,2));
            console.log('Team won-toss won-match  is written to file');        
        });
    }catch(error){
        console.error("Error:",error);
    }
    
}
getTeamWonTossWonMatch();