const csvToJsonConvertion = require("../csvToJson");
const fs = require('fs');

const matchCsvFilePath= '../data/matches.csv';
const deliveriesCsvFilePath = '../data/deliveries.csv';
const outputFilePath = '../public/output/top10EconomicalBowlers.json';

function getTopEconomicBowlers(){
    try{ 
        csvToJsonConvertion(matchCsvFilePath).then((matchData)=>{
            
            const matchIds=[];
            for(let index=0;index<matchData.length;index++){
                if(matchData[index].season==='2015'){
                    matchIds.push(matchData[index].id);
                }
            }
            csvToJsonConvertion(deliveriesCsvFilePath).then((deliverieData)=>{
                    const bowlerStatsOfSeason = {};
                    for(let index=0;index<deliverieData.length;index++){
                        const delivery = deliverieData[index];
                        const matchId =  delivery.match_id;
                        const isWide = delivery.wide_runs && parseInt(delivery.wide_runs) > 0 ;
                        const isNoBall = delivery.noball_runs && parseInt(delivery.noball_runs) > 0 ;

                        if(matchIds.includes(matchId) && !isWide && !isNoBall){
                            const bowler = delivery.bowler;
                            const totalRuns = parseInt(delivery.total_runs);
                            if(bowlerStatsOfSeason[bowler]){
                                bowlerStatsOfSeason[bowler].balls++;
                            } else{
                                bowlerStatsOfSeason[bowler] = {runs: 0 , balls: 1};
                            }
                            bowlerStatsOfSeason[bowler].runs+=totalRuns;
                        }
                    }
                    //console.log(bowlerStatsOfSeason);
                    
                    const economyRates = {};
                    const bowlers= Object.keys(bowlerStatsOfSeason);
                    for(let index=0;index<bowlers.length;index++){
                            const bowler = bowlers[index];
                            const runs = bowlerStatsOfSeason[bowler].runs;
                            const balls = bowlerStatsOfSeason[bowler].balls;
                            economyRates[bowler]=(runs/balls)*6;
                    }
                    //console.log(economyRates);
                    const  sortedBowlers= Object.keys(economyRates).sort((a,b)=> economyRates[a]-economyRates[b]);
                    const top10EconomicalBowlers = sortedBowlers.slice(0,10);
                    fs.writeFileSync(outputFilePath,JSON.stringify(top10EconomicalBowlers,null,2));
                    console.log('Top 10 economy bowler in year 2015 is written to file');
            });
            
        });
    }catch(error){
        console.error("Error:", error );
    }
}
getTopEconomicBowlers();