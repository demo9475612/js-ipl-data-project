
const csvToJsonConvertion = require("../csvToJson");
const fs = require('fs');

const matchCsvFilePath= '../data/matches.csv';
const outputFilePath = '../public/output/highestPlayerOfMatchAwardsPerSeason.json';

function getHighestPlayerOfMatchAward(){
    try{
        const matchData= csvToJsonConvertion(matchCsvFilePath).then((matchData)=>{
             const playerOfMatchAwardsStats ={};
             for(let index=0;index<matchData.length;index++){
                const season = matchData[index].season;
                const playerOfMatch = matchData[index].player_of_match;

                if(!playerOfMatchAwardsStats[season]){
                     playerOfMatchAwardsStats[season]={};
                }
                if(playerOfMatchAwardsStats[season][playerOfMatch]){
                    playerOfMatchAwardsStats[season][playerOfMatch]++;
                }else{
                    playerOfMatchAwardsStats[season][playerOfMatch]=1;
                }
             }
             //console.log(playerOfMatchAwardsStats);
             const playerOfMatchAwards ={};
             for(let season in playerOfMatchAwardsStats ){
                  const poM= Object.fromEntries(Object.entries(playerOfMatchAwardsStats[season]).sort((a,b)=>b[1]-a[1]).slice(0,1));
                  playerOfMatchAwards[season]=poM;

             }
            // console.log(playerOfMatchAwards);
             fs.writeFileSync(outputFilePath,JSON.stringify(playerOfMatchAwards,null ,2));
             console.log("Highest Player Of Match Award Player is written in File");

        });
    }catch(error){
        console.error("Error:",error);
    }
}
getHighestPlayerOfMatchAward();