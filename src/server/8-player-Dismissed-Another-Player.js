const { get } = require("lodash");
const csvToJsonConvertion = require("../csvToJson");
const fs = require('fs');

const matchCsvFilePath= '../data/matches.csv';
const deliveriesCsvFilePath = '../data/deliveries.csv';
const outputFilePath = '../public/output/highestTimeOnePlayerDismissedByAnother.json';

function getPlayerDismissedByAnotherPlayer(){
    try{ 
       
        csvToJsonConvertion(deliveriesCsvFilePath).then((deliveriesData)=>{
            const playerDismissalsCount= {};
            for(let index=0;index<deliveriesData.length;index++){
                const delivery = deliveriesData[index];
                const batsman = delivery.batsman;
                const bowler = delivery.bowler;
                const player_dismissed=delivery.player_dismissed;
                const dismissal_kind = delivery.dismissal_kind;

                if(dismissal_kind && dismissal_kind!== 'run out'){
                    // if(!playerDismissalsCount[batsman]){
                    //     playerDismissalsCount[batsman]={};
                    // }
                    // if(!playerDismissalsCount[batsman][bowler]){
                    //     playerDismissalsCount[batsman][bowler]=1;
                    // }else {
                    //     playerDismissalsCount[batsman][bowler]++;
                    // }

                    const pairCount = batsman+' By '+bowler;
                    playerDismissalsCount[pairCount]=(playerDismissalsCount[pairCount] || 0)+1;
                }

            }
            let maxDismissals =0;
            let maxFrequentDismissals=null;
            for(let data in playerDismissalsCount){
                if(playerDismissalsCount[data]>maxDismissals){
                    maxDismissals=playerDismissalsCount[data];
                    maxFrequentDismissals=data;
                }
            }
            const maxDismissalsByAnother = `Max Dismissals :${maxDismissals}  and Max Frequent Dismissals is :${maxFrequentDismissals}} `;
           // console.log(maxDismissals,maxFrequentDismissals);
           fs.writeFileSync(outputFilePath,JSON.stringify(maxDismissalsByAnother,null,2));
           console.log(maxDismissalsByAnother);
        });
    }catch(error){
        console.error("Error Found:", error);
    }
}
getPlayerDismissedByAnotherPlayer();