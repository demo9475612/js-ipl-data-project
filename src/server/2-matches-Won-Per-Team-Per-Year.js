const csvToJsonConvertion = require("../csvToJson");
const fs = require('fs');

const matchCsvFilePath= '../data/matches.csv';
const outputFilePath = '../public/output/matchesWonPerTeamPerYear.json';
function getMatchesWonPerTeamPerYear(){
    const matchData= csvToJsonConvertion(matchCsvFilePath).then((matchData)=>{
        const matchesWonPerTeamPerYear={};
        if(Array.isArray(matchData)){
            for(index=0; index<matchData.length; index++){
                const season = matchData[index].season;
                const winnerOfGame = matchData[index].winner;
                if(winnerOfGame==""){
                    continue;
                }
    
                if(!matchesWonPerTeamPerYear[season]){
                    matchesWonPerTeamPerYear[season]={};
                }
                if(!matchesWonPerTeamPerYear[season][winnerOfGame]){
                    matchesWonPerTeamPerYear[season][winnerOfGame]=1;
                }else {
                    matchesWonPerTeamPerYear[season][winnerOfGame]++;
                }
            }
            //console.log(matchesWonPerTeamPerYear);
            fs.writeFileSync(outputFilePath,JSON.stringify(matchesWonPerTeamPerYear,null,2));
            console.log('Number of matches won per team per year data written to file');
        }else {
            console.log('Error : Array is Not Found');
        }
        
    });
}
getMatchesWonPerTeamPerYear();