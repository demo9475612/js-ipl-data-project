
const csvToJsonConvertion = require("../csvToJson");
const fs = require('fs');

const matchCsvFilePath= '../data/matches.csv';
const outputFilePath = '../public/output/matchesPerYear.json';
function getMatchesPerYear(){
    const matchData= csvToJsonConvertion(matchCsvFilePath).then((matchData)=>{
        if(Array.isArray(matchData)){
            const matchesPerYear = {};

            for(let index=0;index<matchData.length;index++){
            
                const season = matchData[index].season;
                if(matchesPerYear[season]!== undefined){
                    matchesPerYear[season]++;
                }else {
                    matchesPerYear[season]=1;
                }
            }
            // console.log(matchesPerYear);

            fs.writeFileSync(outputFilePath,JSON.stringify(matchesPerYear,null,2));
            console.log('Number of matches per year data written to file');
        }else {
            console.log('Error : Array is Not Found');
        }
        
        
    });
}
getMatchesPerYear();
