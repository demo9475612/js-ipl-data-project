const { match } = require("assert");
const csvToJsonConvertion = require("../csvToJson");
const fs = require('fs');

const matchCsvFilePath= '../data/matches.csv';
const deliveriesCsvFilePath = '../data/deliveries.csv';
const outputFilePath = '../public/output/extraRunsConcededPerTeam2016.json';

function getExtraRunsConceded(year){
    const matchData= csvToJsonConvertion(matchCsvFilePath).then((matchData)=>{
        const matchIdToSeason = {};
        for(index =0 ;index<matchData.length;index++){
            const match = matchData[index];
            matchIdToSeason[match.id]=match.season;
        }
       
       const deliveriesData= csvToJsonConvertion(deliveriesCsvFilePath).then((deliveriesData)=>{
            const extraRunsConceded ={};
            for(index=0;index<deliveriesData.length;index++){
                const delivey= deliveriesData[index];
                //console.log(delivey);
                const matchId = delivey.match_id;
                const season = Number.parseInt(matchIdToSeason[matchId]);
                if(season===year){
                    const bowlingTeam=delivey.bowling_team;
                    const extraRun = Number.parseInt(delivey.extra_runs);
                    if(extraRunsConceded[bowlingTeam]){
                        extraRunsConceded[bowlingTeam]+=extraRun;
                    }else{
                        extraRunsConceded[bowlingTeam]=extraRun;
                    }
                }
            }
            fs.writeFileSync(outputFilePath,JSON.stringify(extraRunsConceded,null,2));
            console.log('Extra Run conceded per Team in 2016 is written to file');

       });


    });

}
getExtraRunsConceded(2016);