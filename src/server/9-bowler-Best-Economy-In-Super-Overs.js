const csvToJsonConvertion = require("../csvToJson");
const fs = require('fs');

const matchCsvFilePath= '../data/matches.csv';
const deliveriesCsvFilePath = '../data/deliveries.csv';
const outputFilePath = '../public/output/bestEconomyBowlerInSuperOver.json';

function getBestEconomyBowlerInSuperOver(){
    try{ 
       
        csvToJsonConvertion(deliveriesCsvFilePath).then((deliveriesData)=>{
            const bowlerStats ={};
            for(let index=0;index<deliveriesData.length;index++){
                const delivery=deliveriesData[index];
                const bowler = delivery.bowler;
                const runs = parseInt(delivery.total_runs);
                const isSuperOver = delivery.is_super_over==='1';
                const isWide = delivery.wide_runs && parseInt(delivery.wide_runs) > 0;
                const isNoBall = delivery.noball_runs && parseInt(deliveriesData.noball_runs) > 0;

                if(isSuperOver && !isWide && !isNoBall){
                    bowlerStats[bowler]= bowlerStats[bowler] || {runs:0, balls:0};
                    bowlerStats[bowler].runs +=runs;
                    bowlerStats[bowler].balls++;
                }

            }
            //console.log(bowlerStats);
            let bestEconomyBowler =null;
            let minEconomy =Infinity;

            const bowlers =Object.keys(bowlerStats);
            for(let index=0;index<bowlers.length;index++){
               const bowler = bowlers[index];
               const economy= (bowlerStats[bowler].runs/bowlerStats[bowler].balls)*6;
               if(economy<minEconomy){
                minEconomy=economy;
                bestEconomyBowler=bowler;
               }
            }
            //console.log(bestEconomyBowler);
            fs.writeFileSync(outputFilePath,JSON.stringify(bestEconomyBowler,null,2));

        });
    }catch(error){
        console.error("Error Found : ", error);
    }
}
getBestEconomyBowlerInSuperOver();
