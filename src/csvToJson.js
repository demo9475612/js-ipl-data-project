const fs = require('fs');
const csv = require('csvtojson');


function csvToJsonConvertion(csvFilePath){
   return csv()
   .fromFile(csvFilePath)
   .then((jsonObj)=>{
      return jsonObj;
   })
  
}

module.exports = csvToJsonConvertion;